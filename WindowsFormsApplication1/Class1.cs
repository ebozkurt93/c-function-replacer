﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    class Class1
    {
        public string fileLocation { get; set; }//= "C:\\Users\\erdem.bozkurt\\Desktop\\test_file.aspx.cs";
        private string allFile;
        int leftCurlyBraceCount = 0;
        int rightCurlyBraceCount = 0;
        bool inSingleLineComment;
        bool inMultiLineComment;
        bool inSingleQuotationMarks;
        bool inDoubleQuotationMarks;
        private int indexCounter;
        //[\s](protected void \S*\s*\(object.*,.*RadComboBoxSelectedIndexChangedEventArgs.*\)[^{]*{)([^{]\s*)(.{3})
        //private string expression = "[\\s](protected void \\S*\\s*\\(object.*,.*RadComboBoxSelectedIndexChangedEventArgs.*\\)[^{]*{)([^{]\\s*)(.{3})";
        public string expression { get; set; }// = "[\\s](protected void \\S*\\s*_Click\\(object.*,.*EventArgs.*\\)[^{]*{)([^{]\\s*)(.{3})";
        public List<string> changedFileList { get; set; }
        public int otherFuncCount { get; set; }

        [System.Obsolete]
        private void initialize()
        {
            //System.Diagnostics.Debug.WriteLine(text);
            //Regex regex = new Regex("(protected void btn.*_Click\\(object.*,.*EventArgs.*\\)[^{]*{)[^{]\\s*");
            Match mc = Regex.Match(allFile, "(protected void btn.*_Click\\(object.*,.*EventArgs.*\\)[^{]*{)([^{]\\s*)([A-Za-z0-9]{3})");
            if (mc != null)
            {
                //System.Diagnostics.Debug.WriteLine(mc);
                //System.Diagnostics.Debug.WriteLine("Index: " + m.Index);
                if (!mc.Groups[3].Value.Equals("try"))
                {
                    string allAfterIndex = allFile.Substring(mc.Groups[2].Index + mc.Groups[2].Length);
                    //System.Diagnostics.Debug.WriteLine("Rest of the text: " + allAfterIndex);

                    StringBuilder strBuilder = new StringBuilder();

                    leftCurlyBraceCount = 1;
                    rightCurlyBraceCount = 0;

                    allFile = allFile.Insert(mc.Groups[2].Index + mc.Groups[2].Length, "\ntry\n{\n");
                    indexCounter = mc.Groups[2].Index + mc.Groups[2].Length;

                    foreach (char c in allAfterIndex)
                    {
                        indexCounter++;
                        if (c == '{')
                        {
                            leftCurlyBraceCount++;
                        }
                        if (c == '}')
                        {
                            rightCurlyBraceCount++;
                        }
                        if (leftCurlyBraceCount == rightCurlyBraceCount)
                        {
                            allFile = allFile.Insert(indexCounter - 1, "\n}\ncatch (Exception ex)\n{\nthis.PublishException(ex);\n}\n");
                            File.WriteAllText(fileLocation, allFile);
                            //System.Diagnostics.Debug.WriteLine(allFile);
                            rightCurlyBraceCount = 5000; //to break the loop where in each empty space leftCurlyBraceCount and rightCurlyBraceCount will be equal
                        }
                        allFile = File.ReadAllText(fileLocation);
                    }
                } //else already has try/catch block, skip this func



                MessageBox.Show("Finished");

            }
            else MessageBox.Show("Didn't match anything");
        }

        private void initializeAllValues()
        {
            //"iso-8859-9"
            //allFile = File.ReadAllText(fileLocation, Encoding.GetEncoding("UTF-8"));
            leftCurlyBraceCount = 0;
            rightCurlyBraceCount = 0;
            indexCounter = 0;
            inSingleLineComment = false;
            inMultiLineComment = false;
            inSingleQuotationMarks = false;
            inDoubleQuotationMarks = false;
        }

        private Match findFirstMatchingValue()
        {
            //System.Diagnostics.Debug.WriteLine(text);
            //Regex regex = new Regex("(protected void btn.*_Click\\(object.*,.*EventArgs.*\\)[^{]*{)[^{]\\s*");
            Match mc = Regex.Match(allFile, "(protected void btn.*_Click\\(object.*,.*EventArgs.*\\)[^{]*{)([^{]\\s*)([A-Za-z0-9]{3})");
            return mc;
        }

        private MatchCollection findAllMatchingValues()
        {
            //System.Diagnostics.Debug.WriteLine(text);
            //Regex regex = new Regex("(protected void btn.*_Click\\(object.*,.*EventArgs.*\\)[^{]*{)[^{]\\s*");
            MatchCollection mc = Regex.Matches(allFile, expression);
            return mc;
        }

        private int findNumberOfMatches()
        {
            //System.Diagnostics.Debug.WriteLine(text);
            //Regex regex = new Regex("(protected void btn.*_Click\\(object.*,.*EventArgs.*\\)[^{]*{)[^{]\\s*");
            if (!Regex.IsMatch(allFile, expression))
            {
                return 0;
            }
            MatchCollection mc = Regex.Matches(allFile, expression);
            return mc.Count;
        }

        private bool hasTryCatchBlock(Match mc)
        {
            if (mc != null)
            {
                if (mc.Groups[3].Value.Equals("try") || mc.Groups[3].Value.Contains("}"))
                {
                    return true;
                }
                else return false;

            }
            return true;

            /*
            if (mc != null && mc.Groups[3].Value.Equals("try"))
            {
                return false;
            }
            else return true;
            */
        }

        private void updateFunction(Match mc)
        {
            indexCounter = mc.Groups[2].Index + mc.Groups[2].Length;
            string allAfterIndex = allFile.Substring(indexCounter);

            StringBuilder strBuilder = new StringBuilder();

            leftCurlyBraceCount = 1;
            rightCurlyBraceCount = 0;

            //allFile = allFile.Insert(indexCounter, "\n\t\t\ttry\n\t\t\t{\n\t\t\t\t");
            string addToStart = "try{\n";
            addToStart = addToStart.Replace("\n", System.Environment.NewLine);
            allFile = allFile.Insert(indexCounter, addToStart);
            //System.Diagnostics.Debug.WriteLine(allFile);



            foreach (char c in allAfterIndex)
            {
                char[] allChars = allFile.ToCharArray();

                if (inSingleLineComment == false && inMultiLineComment == false && inSingleQuotationMarks == false && inDoubleQuotationMarks == false)
                {
                    if (c == '/')
                    {
                        if (allChars[indexCounter + 1] == '*')
                        {
                            inMultiLineComment = true;
                        }
                        else if (allChars[indexCounter - 1] == '/')
                        {
                            inSingleLineComment = true;
                        }
                    }
                    else if (c == '\'')
                    {
                        inSingleQuotationMarks = true;
                    }
                    else if (c == '"')
                    {
                        inDoubleQuotationMarks = true;
                    }

                    else if (c == '{' && inSingleLineComment == false && inMultiLineComment == false && inSingleQuotationMarks == false && inDoubleQuotationMarks == false)
                    {
                        leftCurlyBraceCount++;
                        //string asd = allFile.Substring(indexCounter, 300);

                        // System.Diagnostics.Debug.WriteLine(asd + "\n--------------------------------------------------------------\n");
                    }
                    else if (c == '}' && inSingleLineComment == false && inMultiLineComment == false && inSingleQuotationMarks == false && inDoubleQuotationMarks == false)
                    {
                        rightCurlyBraceCount++;
                        //string asd = allFile.Substring(indexCounter, 300);
                        //System.Diagnostics.Debug.WriteLine(asd + "\n--------------------------------------------------------------\n");
                        //  System.Diagnostics.Debug.WriteLine(asd + "\n--------------------------------------------------------------\n");
                    }

                }
                if (inSingleLineComment == true)
                {
                    if (c == '\n')
                    {
                        inSingleLineComment = false;
                    }
                }
                if (inMultiLineComment == true)
                {
                    if (c == '*' && allChars[indexCounter + 1] == '\\')
                    {
                        inMultiLineComment = false;
                    }
                }
                if (inSingleQuotationMarks == true)
                {
                    if (c == '\'' && allChars[indexCounter - 1] != '\\')
                    {
                        inSingleQuotationMarks = false;
                    }
                }
                if (inDoubleQuotationMarks == true)
                {
                    if (c == '"' && allChars[indexCounter - 1] != '\\')
                    {
                        inDoubleQuotationMarks = false;
                    }
                }
                indexCounter++;

                if (leftCurlyBraceCount == rightCurlyBraceCount)
                {
                    //indexCounter++;
                    //string test = allFile.Substring(indexCounter, 550);
                    //System.Diagnostics.Debug.WriteLine(test);


                    // you can use the index here to update function, than break foreach loop
                    //allFile = allFile.Insert(indexCounter - 1, "}\ncatch (Exception ex)\n{\nthis.PublishException(ex);\n}\n");
                    //string addToEnd = "}\ncatch (Exception ex){this.PublishException(ex);}";
                    string addToEnd = "}\ncatch (Exception ex){this.Page.PublishException(ex);}";
                    addToEnd = addToEnd.Replace("\n", System.Environment.NewLine);
                    allFile = allFile.Insert(indexCounter - 1, addToEnd);
                    //File.WriteAllText(fileLocation, allFile, Encoding.GetEncoding("UTF-8"));
                    //System.Diagnostics.Debug.WriteLine(allFile);
                    rightCurlyBraceCount = 5000; //to break the loop where in each empty space leftCurlyBraceCount and rightCurlyBraceCount will be equal
                    break;
                }

                //indexCounter++;

            }
        }


        public void runOnce(Match m)
        {
            initializeAllValues();
            //System.Diagnostics.Debug.WriteLine(m.Value);
            //System.Diagnostics.Debug.WriteLine(hasTryCatchBlock(m));
            if (!hasTryCatchBlock(m))
            {
                //System.Diagnostics.Debug.WriteLine("in loop");
                otherFuncCount++;
                updateFunction(m);
                bool alreadyExists = changedFileList.Contains(fileLocation);
                if (!alreadyExists)
                    changedFileList.Add(fileLocation);
            }
        }

        public void runFile()
        {
            FileAttributes attributes = File.GetAttributes(fileLocation);
            if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
            {
                // Make the file RW
                attributes = RemoveAttribute(attributes, FileAttributes.ReadOnly);
                File.SetAttributes(fileLocation, attributes);
            }
            initializeAllValues();
            allFile = File.ReadAllText(fileLocation, Encoding.GetEncoding("UTF-8"));
            //System.Diagnostics.Debug.WriteLine(findNumberOfMatches());

            for (int i = 0; i < findNumberOfMatches(); i++)
            {
                MatchCollection matches = findAllMatchingValues();
                Match m = matches[i];
                //System.Diagnostics.Debug.WriteLine(m);

                runOnce(m);
                //m = m.NextMatch();
            }
            File.WriteAllText(fileLocation, allFile, Encoding.GetEncoding("UTF-8"));
        }


        private static FileAttributes RemoveAttribute(FileAttributes attributes, FileAttributes attributesToRemove)
        {
            return attributes & ~attributesToRemove;
        }


        public void func()
        {


            int leftCurlyBraceCount = 0;
            int rightCurlyBraceCount = 0;
            bool inSingleLineComment = false;
            bool inMultiLineComment = false;
            bool inSingleQuotationMarks = false;
            bool inDoubleQuotationMarks = false;
            string allAfterIndex = ""; //TODO: remove this 
            foreach (char c in allAfterIndex)
            {
                char[] allChars = allFile.ToCharArray();

                if (inSingleLineComment == false && inMultiLineComment == false && inSingleQuotationMarks == false && inDoubleQuotationMarks == false)
                {
                    if (c == '/')
                    {
                        if (allChars[indexCounter + 1] == '*')
                        {
                            inMultiLineComment = true;
                        }
                        else if (allChars[indexCounter + 1] == '/')
                        {
                            inSingleLineComment = true;
                        }
                    }
                    else if (c == '\'')
                    {
                        inSingleQuotationMarks = true;
                    }
                    else if (c == '"')
                    {
                        inDoubleQuotationMarks = true;
                    }

                    else if (c == '{')
                    {
                        leftCurlyBraceCount++;
                    }
                    else if (c == '}')
                    {
                        rightCurlyBraceCount++;
                    }

                }
                else if (inSingleLineComment == true)
                {
                    if (c == '\n')
                    {
                        inSingleLineComment = false;
                    }
                }
                else if (inMultiLineComment == true)
                {
                    if (c == '*' && allChars[indexCounter + 1] == '\\')
                    {
                        inMultiLineComment = false;
                    }
                }
                else if (inSingleQuotationMarks == true)
                {
                    if (c == '\'' && allChars[indexCounter - 1] != '\\')
                    {
                        inSingleQuotationMarks = false;
                    }
                }
                else if (inDoubleQuotationMarks == true)
                {
                    if (c == '"' && allChars[indexCounter - 1] != '\\')
                    {
                        inDoubleQuotationMarks = false;
                    }
                }

                if (leftCurlyBraceCount == rightCurlyBraceCount)
                {
                    // you can use the index here to update function, than break foreach loop
                    //allFile = allFile.Insert(indexCounter - 1, "}\ncatch (Exception ex)\n{\nthis.PublishException(ex);\n}\n");
                    allFile = allFile.Insert(indexCounter - 1, "}\ncatch (Exception ex){this.PublishException(ex);}");
                    File.WriteAllText(fileLocation, allFile, Encoding.GetEncoding("UTF-8"));
                    //System.Diagnostics.Debug.WriteLine(allFile);
                    rightCurlyBraceCount = 5000; //to break the loop where in each empty space leftCurlyBraceCount and rightCurlyBraceCount will be equal
                    break;
                }

                indexCounter++;

            }
        }


    }

}
