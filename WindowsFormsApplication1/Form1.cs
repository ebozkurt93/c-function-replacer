﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        OpenFileDialog ofd = new OpenFileDialog();
        ArrayList fileNames = new ArrayList();


        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            int count = 0;
            Class1 a = new Class1();
            a.expression = regex.Text;
            var path = txtPath.Text;
            textBox1.Clear();

            var files = Directory.GetFiles(path, fileFormat.Text, SearchOption.AllDirectories);
            a.changedFileList = new List<string>();

            foreach (String file in files)
            {
                try
                {
                    a.fileLocation = file;
                    a.runFile();
                    count++;
                    textBox1.AppendText("Controlled " + file + "\n");
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Failed file: " + file);
                    textBox1.AppendText("Failed file: " + file + "\nException: " + ex + "\n");
                }
            }

            /*
            foreach (String file in fileNames)
            {
                try
                {
                    a.fileLocation = file;
                    a.runFile();
                    count++;
                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("Failed file: " + file);
                    textBox1.AppendText("Failed file: " + file + "\nException: " + ex + "\n");
                }
            }*/
            /*
            TextWriter tw = new StreamWriter("C:\\Users\\erdem.bozkurt\\Desktop\\SavedList.txt");

            foreach (String s in a.changedFileList)
                tw.WriteLine(s);

            tw.Close();
            */
            MessageBox.Show($"Done, checked {count} items, updated {a.otherFuncCount} functions.");

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            fileNames.Clear();
            ofd.Filter =
                "aspx.cs files (*aspx.cs)|*aspx.cs*";
            ofd.Multiselect = true;
            ofd.Title = "Select some files";
            if (ofd.ShowDialog() == DialogResult.OK)
            {

                //MessageBox.Show(ofd.FileName.ToString());
                foreach (String file in ofd.FileNames)
                {
                    fileNames.Add(file);
                    //System.Diagnostics.Debug.WriteLine(file);

                }
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
